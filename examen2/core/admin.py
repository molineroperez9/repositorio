from django.contrib import admin

from .models import City, Stadium, Team


@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "estadio",
        "nombre",
        
    ]

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "equipo",
        "descripcion",
    ]

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "ciudad",
    ]
